package ru.t1k.vbelkin.tm.repository;

import ru.t1k.vbelkin.tm.api.repository.IRepository;
import ru.t1k.vbelkin.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }


    @Override
    public M findOneByIndex(Integer index) {
        return models.get(index);
    }

    @Override
    public Integer getSize() {
        return models.size();
    }

    @Override
    public M remove(M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

}
